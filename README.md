#travelTopic
##说明

###originalData
>原始数据

###cleanData
>清理后数据

###code
>代码
代码可运行，文件路径均为相对路径。

###code/image
>可视化代码，文件路径均为相对路径。

###gloveWordVector
>glove词向量

###word2vecWordVector
>word2vec词向量

###utilTxt
>工具字典

###image
>可视化结果

###noteBook
>运行笔记

###model
>模型及部分缓存词向量