# 1.删除不是字符串和小于5的
# 2.删除delKeyWord数组中的关键字
# 3.删除标点符号
# 4.删除表情
# 5.删除停用词
# 6.去除重复
# 7.去除某些值
###############################################
import re
import logging
import pandas as pd
import jieba as jb
import warnings

warnings.filterwarnings("ignore")  # 忽略版本问题

inFilePath='../originalData/sc_pos.xlsx'
outFilePath='../cleanData/data_sc_pos.txt'
dataset=pd.read_excel(inFilePath,header=None) #输入文件
a=0
##正则化去除标点符号
r4 = "\\【.*?】+|\\《.*?》+|\\#.*?#+|[．.!/_,$&%^*()<>+""'?@|:~{}#]+|[—— - ！\\\，,。=？;←→、；⛲ ➕ ☺ ㉨ ● ☔ ✌ ⛴❗ ❤ ⏱ ⛰ ♥ ⛪ ⛅☀：★ ❣ … －－－ ～ ---「」 ⋯“”‘'￥……（）丶·^ * % @ 《》【】⭐ a-zA-Z0-9]"
#存放去除评论的索引值
listt=[]
delKeyWord= ['微信', '拼车', 'wx','组队','同行']
for i in range(len(dataset)):#一行一行读取

    StrComment=dataset[0][i]
    logging.info('删除不是字符串和小于5的评价')
    if type(StrComment)!=str or len(StrComment)<=5:
        listt.append(i)
        continue
    logging.info('删除出现关键词的评价')
    for j in delKeyWord:
        searchStr=StrComment.find(j)
        if searchStr != -1:
            listt.append(i)
            break
    ##剔除r4  正则化表达式过滤
    StrClean=re.sub(r4,'',StrComment)
    ##再次应用正则化剔除除文字外的评价
    StrClean_1= re.sub(u"([^\u4e00-\u9fa5])", "", StrClean)
    # 通过unicode过滤表情
    try:
        res = re.compile(u'[\U00010000-\U0010ffff]')
    except re.error:
        res = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
    dataFinalClean=res.sub(u'',StrClean_1)
    ##重新赋给评价
    dataset[0][i]=dataFinalClean
    #再次判断字符长度是否小于5
    if type(dataFinalClean)==float or len(dataFinalClean)<=5:
        listt.append(i)
    a+=1
    print(a)

dataset1=dataset.drop(listt,axis=0)
#dataset1.to_excel(inFilePath) #输出清洗后的


#################################################
f=open('../utilTxt/停用词典.txt', 'r')#停止标点符号字典
data=open(outFilePath, 'w')#
##读取停用词表
stopDict=f.read()
#分词与加载词典
##自定义词典
jb.load_userdict("../utilTxt/情感词典.txt")
# dataset=pd.read_excel(inFilePath)
dataCommet=dataset[0]
for k in range(len(dataCommet)):
    words=jb.lcut(dataCommet[k],cut_all=False)
    #取出停用词
    for i in words:
        if i in stopDict:
            words.remove(i)

    #去除重复词
    argWord=list(set(words))
    argWord.sort(key=words.index)
    lenarglist = len(argWord)
    # print(lenarglist)
    #提取评价数据 每个词空一格
    for z in range(lenarglist):
        ##lenarglist放入for会少1
        # print("l", argWord[z])
        if z<lenarglist-1:
            data.writelines(argWord[z]+' ')
        elif z==lenarglist-1:
            data.writelines(argWord[z]+'\n')
    print(k)