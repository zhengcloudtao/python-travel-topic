#折线图
import matplotlib.pyplot as plt
squares=[0.8315,0.8363,0.8375,0.8397,0.8383,0.8386]
x=[1, 2, 3, 4, 5,6]
plt.plot(x, squares)
plt.rcParams['font.sans-serif']=['SimHei'] #解决中文字符乱码
plt.title('glove_四省数据_不同维度_SVC评分')
plt.savefig("../../image/折线图.jpg")
plt.show()