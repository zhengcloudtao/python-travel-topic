#双坐标图
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('mathtext', default='regular')

vector = [50,100,150,200,250,300]
word2vec = [0.8350,0.8382,0.8410,0.8419,0.8419,0.8397 ]
glove =[0.8315,0.8363,0.8375,0.8397,0.8383,0.8386]

fig = plt.figure()
ax = fig.add_subplot(111)
plt.rcParams['font.sans-serif'] = 'SimHei'
plt.rcParams['axes.unicode_minus'] = False ## 设置正常显示符号
lns1 = ax.plot(vector, word2vec, '-', label = 'word2vec')
lns2 = ax.plot(vector, glove, '-', label = 'glove')
ax2 = ax.twinx()

lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)

ax.grid()
ax.set_xlabel("维度")
ax.set_ylabel(r"word2vec评分")
ax2.set_ylabel(r"glove评分")
plt.title("四省数据不同维度不同词向量的评分")
ax.set_ylim(0.8310,0.8425)
ax2.set_ylim(0.8312, 0.8400)
plt.savefig("../../image/双坐标图.jpg")
plt.show()
