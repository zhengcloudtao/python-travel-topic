#直方图多图叠加
import numpy as np
import matplotlib.pyplot as plt

N = 6
SVC = (0.8350,0.8382,0.8410,0.8419,0.8419,0.8397)
logistic = (0.8232,0.8288,0.8297,0.8301,0.8295,0.8284)

d = []
for i in range(0, len(SVC)):
    sum = SVC[i] + logistic[i]
    d.append(sum)
naivenayesian = (0.7746,0.7755,0.7784,0.7780,0.7818,0.7776)
# menStd = (2, 3, 4, 1, 2)
# womenStd = (3, 5, 2, 3, 3)
ind = np.arange(N)  # the x locations for the groups
width = 0.35  # the width of the bars: can also be len(x) sequence
plt.rcParams['font.sans-serif'] = 'SimHei'
p1 = plt.bar(ind, SVC, width, color='#d62728')  # , yerr=menStd)
p2 = plt.bar(ind, logistic, width, bottom=logistic)  # , yerr=womenStd)
p3 = plt.bar(ind, naivenayesian, width, bottom=d)
plt.rcParams['axes.unicode_minus'] = False ## 设置正常显示符号

plt.ylabel('评分')
plt.xlabel("维度")
plt.title('四省数据不同维度不同评分')
plt.xticks(ind, ('50', '100', '150', '200', '250', '300'))
plt.yticks(np.arange(0, 3, 0.5))
plt.legend((p1[0], p2[0], p3[0]), ('SVC', 'logistic', 'naivenayesian'))
plt.savefig("../../image/直方图多图叠加.jpg")
plt.show()