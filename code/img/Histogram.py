#直方图
import matplotlib.pyplot as plt

salary = [0.8350,0.8382,0.8410,0.8419,0.8419,0.8397 ]

group = [0.833,0.835,0.837,0.839,0.841,0.843,0.845]

plt.rcParams['font.sans-serif'] = 'SimHei'
plt.rcParams['axes.unicode_minus'] = False ## 设置正常显示符号
plt.hist(salary, group, histtype='bar', rwidth=0.8)
plt.legend()
plt.xlabel('评分组')
plt.ylabel('评分')
plt.title('Word2Vec_四省数据_不同维度_SVC')
plt.savefig("../../image/直方图.jpg")
plt.show()