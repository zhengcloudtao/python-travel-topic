#扇形图
# 导入matplotlib库
import matplotlib.pyplot as plt
import matplotlib as mp1
# 设置输出文字类型
plt.rcParams['font.sans-serif'] = 'SimHei'
plt.rcParams['axes.unicode_minus'] = False ## 设置正常显示符号
# 设置标签
labels =["广东","贵州","四川","云南"]
# 设置数据
fracs = [4760,1662,3258,4326]
# 画面积图
plt.pie(fracs,labels = labels,autopct = '%.0f%%')
plt.title('四省数据其中各省数据具体占比')
# 可视化呈现
plt.savefig("../../image/扇形图.jpg")
plt.show()