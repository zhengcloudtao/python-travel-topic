#散点图
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import xlrd

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
# matplotlib画图中中文显示会有问题，需要这两行设置默认字体
# 打开文件
workbook = xlrd.open_workbook(r'../../result_data.xls')
#获取sheet2
sheet2_name= workbook.sheet_names()[1]
sheet2 = workbook.sheet_by_name('Sheet1')
x1=[]
y1=[]
x2=[]
y2=[]
sheet2.cell(1,0).value.encode('utf-8')
i=3
while(i<24):
    temp = sheet2.cell(i, 1).value
    x1.append(50)
    y1.append(temp)
    temp=sheet2.cell(i, 2).value
    x1.append(100)
    y1.append(temp)
    temp = sheet2.cell(i, 3).value
    x1.append(150)
    y1.append(temp)
    temp = sheet2.cell(i, 4).value
    x1.append(200)
    y1.append(temp)
    temp = sheet2.cell(i, 5).value
    x1.append(250)
    y1.append(temp)
    temp=sheet2.cell(i, 6).value
    x1.append(300)
    y1.append(temp)
    i+=1

i=3
while(i<24):
    temp = sheet2.cell(i, 9).value
    x2.append(50)
    y2.append(temp)
    temp = sheet2.cell(i, 10).value
    x2.append(100)
    y2.append(temp)
    temp=sheet2.cell(i, 11).value
    x2.append(150)
    y2.append(temp)
    temp = sheet2.cell(i, 12).value
    x2.append(200)
    y2.append(temp)
    temp = sheet2.cell(i, 13).value
    x2.append(250)
    y2.append(temp)
    temp=sheet2.cell(i, 14).value
    x2.append(300)
    y2.append(temp)
    i+=1
# print(y1)
plt.xlabel('维度')
plt.ylabel('评分')
plt.xlim(xmax=350, xmin=0)
plt.ylim(ymax=1, ymin=0.6)

#x1 = [50,100,150,200,250,300,50,100,150,200,250,300,50,100,150,200,250,300,50,100,150,200,250,300,50,100,150,200,250,300]
#y1 = [0.8350,0.8382,0.8410,0.8419,0.8419,0.8397,0.9615,0.9636,0.9632,0.9608,0.9625,0.9604,0.7826,0.7776,0.7856,0.7816,0.7856,0.7816,0.7560,0.7570,0.7519,0.7540,0.7555,0.7555,0.7631,0.7758,0.7773,0.7739,0.7770,0.7766 ]
#x2 = [50,100,150,200,250,300,50,100,150,200,250,300,50,100,150,200,250,300,50,100,150,200,250,300,50,100,150,200,250,300]
#y2 = [0.8315,0.8363,0.8375,0.8397,0.8383,0.8386 ,0.9569,0.9566,	0.9583,0.9548,0.9562,0.9580,0.8006,0.7986,0.7946,0.7926,0.7956,0.7856,0.7509,0.7540,0.7565,0.7550,0.7524,0.7535,0.7273,0.7292,0.7296,0.7327,0.7277,0.7261]
colors1 = '#00CED1'  # 点的颜色
colors2 = '#DC143C'
area = np.pi * 4 ** 2  # 点面积
# 画散点图
plt.scatter(x1, y1, s=area, c=colors1, alpha=0.4, label='Word2Vec')
plt.scatter(x2, y2, s=area, c=colors2, alpha=0.4, label='glove')
plt.plot([0, 9], [9, 0], linewidth='0.5', color='#000000')
plt.legend()
plt.title("四省数据不同维度不同词向量评分")
plt.savefig("../../image/散点图.jpg")
plt.show()